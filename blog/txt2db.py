#coding:utf-8
 
import os
from datetime import date
import time

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "blog.settings")
 
 
import django
if django.VERSION >= (1, 7):
    django.setup()
 
 
def main():
    from model.catagory import Catagory
    from model.article import Article
    from model.comment import Comment
    f = open('rawData.txt',encoding='utf-8')
    for line in f:
        kind = line.split('#')[0]
        if kind == 'article':
            catagory = Catagory.objects.get(id='4')
            title = line.split('#')[1]
            content = line.split('#')[2].strip('\n')
            publish_date=time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.time()))
            Article.objects.create(title=title, content=content, publish_date=publish_date, catagory=catagory)
        elif kind == 'comment':
            article = Article.objects.get(id='1')
            author = line.split('#')[1]
            content = line.split('#')[2]
            comment_date = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.time()))
            Comment.objects.create(author=author, content=content, comment_date=comment_date, article=article)
            
        #Catagory.objects.create(title=title, last_modify_date=date.today())
    f.close()
 
if __name__ == "__main__":
    main()
    print('Ok!')
