from django.http import HttpResponse, Http404
import datetime
from django.template import Context
from django.template.loader import get_template
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect

from model import catagory
from model import article
from model import comment

def index(request):

    catagories = catagory.Catagory.objects.all()

    headFiveArticle = article.Article.objects.order_by('-publish_date')[0:5]
        
    
    return render_to_response('index.htm', {'catagories': catagories, 'articles': headFiveArticle})
