from django.db import models
from model.article import Article

class Comment(models.Model):

    content = models.TextField()
    author = models.CharField(max_length=50)
    comment_date = models.DateTimeField()
    article = models.ForeignKey(Article)

    def __str__(self):
        return self.content
