from django.db import models

# Create your models here.
class Catagory(models.Model):
    title = models.CharField(max_length=50)
    last_modify_date = models.DateTimeField()

    def __str__(self):
        return self.title
