from django.db import models
from model.catagory import Catagory

# Create your models here.

class Article(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()
    publish_date = models.DateTimeField()
    catagory = models.ForeignKey(Catagory)
    
    #for python 3.x 
    def __str__(self):
        return self.title

    # def __unicode__(self):
    #    return self.title
